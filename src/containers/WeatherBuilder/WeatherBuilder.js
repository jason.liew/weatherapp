import React, { useEffect, useState } from 'react';
import axios from '../../axios-weather';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import WeatherInput from '../../components/WeatherInput/WeatherInput';
import WeatherDisplay from '../../components/WeatherDisplay/WeatherDisplay';

const WeatherBuilder = props => {
    const [WeatherInfos, setWeatherInfo] = useState([]);

    const baseQuery = `?APPID=85a93cec63066e901beb71ce87f375a4&units=metric&q=`;

    useEffect(() => {
        return () => {
        }
    }, [])

    const onHandleForm = (e) => {
        e.preventDefault();
        const city = e.target.elements["city"].value;
        if (city.trim().length === 0) {
            return;
        }

        const intervalTime = e.target.elements["selInterval"].value;

        const found = WeatherInfos.findIndex(el => el.city === city);
        if (found < 0) {
            getWeather(city, intervalTime);
        }
        e.target.elements["city"].value = "";
    };

    const onHandleMouseIn = (e, city) => {
        const found = WeatherInfos.findIndex(el => el.city === city);
        let foundObj = WeatherInfos[found];
        foundObj.showDelete = true;
        let prevData = [...WeatherInfos];
        prevData[found] = foundObj;

        setWeatherInfo(prevData);
    };

    const onHandleMouseOut = (e, city) => {
        const found = WeatherInfos.findIndex(el => el.city === city);
        let foundObj = WeatherInfos[found];
        foundObj.showDelete = false;
        let prevData = [...WeatherInfos];
        prevData[found] = foundObj;

        setWeatherInfo(prevData);
    };

    const onHandleRemove = (e, city) => {
        let prevData = [...WeatherInfos];
        const found = prevData.findIndex(el => el.city === city);
        const intId = prevData[found].interval;
        if (intId) {
            clearInterval(intId);
            console.log("clear interval");
        }
        prevData.splice(found, 1);
        setWeatherInfo(prevData);
    };

    const getWeather = (city, intervalTime) => {
        const queryParam = baseQuery + `${city}`;
        axios.get(queryParam)
            .then(res => {
                let tmpWeather = {
                    city: city,
                    interval: null,
                    temperature: res.data.main.temp,
                    weatherDescription: res.data.weather[0].description,
                    weatherIcon: res.data.weather[0].icon,
                    iconUrl: `http://openweathermap.org/img/wn/${res.data.weather[0].icon}@2x.png`,
                    showDelete: false
                };

                const intObj = setInterval(() => {
                    getWeatherTimer(city);
                }, intervalTime*60*1000);

                tmpWeather.interval = intObj;

                console.log("interval:", tmpWeather.interval);

                setWeatherInfo(prevStat => [...prevStat, tmpWeather]);
            })
            .catch(error => {
                console.log(error)
            })
    };

    const getWeatherTimer = (city) => {
        const queryParam = baseQuery + `${city}`;
        axios.get(queryParam)
            .then(res => {
                let tmpWeather = {
                    city: city,
                    interval: 0,
                    temperature: res.data.main.temp,
                    weatherDescription: res.data.weather[0].description,
                    weatherIcon: res.data.weather[0].icon,
                    iconUrl: `http://openweathermap.org/img/wn/${res.data.weather[0].icon}@2x.png`,
                    showDelete: false
                };

                setWeatherInfo(preState => {
                    const dt = [...preState];
                    const found = dt.findIndex(el => el.city === city);
                    dt[found] = tmpWeather;
                    return [...dt];
                });
            })
            .catch(error => {
                console.log(error)
            })
    };

    let weatherInfoDis = null;
    if (WeatherInfos.length > 0) {

        let data = WeatherInfos.map(weather =>
            (<WeatherDisplay key={weather.city} city={weather.city}
                temperature={weather.temperature}
                condition={weather.weatherDescription}
                image={weather.iconUrl}
                mouseIn={onHandleMouseIn}
                mouseOut={onHandleMouseOut}
                showDelete={weather.showDelete}
                removeButton={onHandleRemove} />)
        );

        weatherInfoDis = (
            <React.Fragment>
                {data}
            </React.Fragment>
        );
    }
    return (
        <React.Fragment>
            <div className="container">
                {weatherInfoDis}
            </div>
            <WeatherInput onFormChange={onHandleForm} />
        </React.Fragment>
    );
}

export default withErrorHandler(WeatherBuilder, axios);