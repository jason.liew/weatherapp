import React from 'react';
import classes from './WeatherDisplay.module.css';

const weatherDisplay = props => {
    let delButton = props.showDelete ? (
        <button className="btn-danger" 
            onClick={(e) => props.removeButton(e, props.city)}>Remove</button>) : null;

    return (
        <div onMouseEnter={(e) => props.mouseIn(e, props.city)} 
            onMouseLeave={(e) => props.mouseOut(e, props.city)}
            className={[classes.weatherDisplay, "md-4"].join(" ")}>
            <div className={classes.city}>{props.city.toUpperCase()}</div>
            <img className={classes.img} src={props.image} alt="img" />
            <div className={classes.desc}>Temperature: {props.temperature} &#8451;</div>
            <div className={classes.desc}>Condition: {props.condition} </div>
            {delButton}
        </div>
    );
}

export default weatherDisplay;