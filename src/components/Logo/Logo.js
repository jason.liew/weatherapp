import React from 'react';
import weatherLogo from '../../assets/images/weather-logo.png';
import classes from './Logo.module.css';

const logo = (props) => {
    return (  
        <div className={classes.Logo}>
            <img src={weatherLogo} alt="weatherLogo" />
        </div>
    );
}
 
export default logo;
