import React from 'react';
import classes from './WeatherInput.module.css';

const weatherInput = (props) => (
    <form onSubmit={(e)=> props.onFormChange(e)}>
        <div className={classes.BuildControls}>
            <div className={classes.BuildControl}>
                <input className="form-control"
                    style={{ margin: "10px" }}
                    type="text" 
                    name="city"
                    placeholder="Enter city name" />
                <select className="form-control" name="selInterval">
                    <option value="0">Select a refresh interval</option>
                    <option value="30">30 minutes</option>
                    <option value="60">60 minutes</option>
                    <option value="120">120 minutes</option>
                </select>
            </div>
            <button className="btn-primary">Add City</button>
        </div>
    </form>
);

export default weatherInput;