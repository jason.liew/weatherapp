/* eslint-disable import/no-anonymous-default-export */
import { useLayoutEffect, useState } from 'react';

export default useHttpClient => {

    const [error, setError] = useState(null);

    useLayoutEffect(() => {
        const reqInterceptor = useHttpClient.interceptors.request.use(req => {
            setError(null);
            return req;
        });
        
        const resInterceptor = useHttpClient.interceptors.response.use(
            res => res,
            err => {
                setError(err);
            }
        );

        return () => {
            useHttpClient.interceptors.request.eject(reqInterceptor);
            useHttpClient.interceptors.response.eject(resInterceptor);
        };
    }, [useHttpClient.interceptors.request, useHttpClient.interceptors.response]);

    const errorConfirmedHandler = () => {
        setError(null);
    };

    return [error, errorConfirmedHandler];
}