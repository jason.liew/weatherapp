import './App.css';
import Layout from './hoc/Layout/Layout';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import WeatherBuilder from './containers/WeatherBuilder/WeatherBuilder';

function App() {

  let routes = (
    <Switch>
      <Route path="/" exact component={WeatherBuilder} />
      <Redirect to='/' />
    </Switch>
  );


  return (
    <div>
      <Layout>
      {routes}
      </Layout>
    </div>
  );
}

export default withRouter(App);
