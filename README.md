Run 'npm install' in the base folder
After the install process, then run 'npm start'
Use the browser to browse to http://localhost:3000
To use:
Key in city name in and select an interval and click Add City.
If interval is not selected or at position 0, then no timer will be set but the weather information is still available.
